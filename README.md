# README

# Use on Rails project
   - For validate the access token use the AuthorizationCheck middleware
        -           class YourController < ApplicationController
                        use TrustGem::AuthorizationCheck
         
                        def create
                        end 

  - Add the next lines on your config/application.rb inside of the class Application
    -     config.middleware.use TraceCapture
    -     ENV['PROJECT_VERSION'] = '1.0.0' 
    -     ENV['PROJECT_NAME'] = YOUR_SERVICE_NAME
  - Check in your config/environments files have a valid cache config, this is a example
    -     config.action_controller.perform_caching = true
          config.cache_store = :memory_store

# local develop
 - create the gem with the command
    - gem build trust_gem.gemspec
    
 - import the local gem into a gemfile using
    - gem "trust_gem", '~> 1.5.0', path: 'path/lat_trust_backend_library_trust_gem/', :tag => 'v1.5.0'



* 25/04/2020 / 1.1.0
  - Added trace helpers

* 27/10/2019 / 1.0.6.3
  - fix get access token method

* 23/05/2019 / 1.0.6
  - Only store in cache the response of oauth server if is 401 (not valid) or 200 (ok).
  - Reduce cache time to 5 minutes for the oauth server response.

* 22/05/2019 / 1.0.5
  - Fix https problem

* / 1.0.4
