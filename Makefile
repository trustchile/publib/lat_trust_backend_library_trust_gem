.PHONY: build_local
build_local:
	gem build trust_gem.gemspec

.PHONY: tag
tag:
	git tag $(version)
	git push origin $(version)

.PHONY: delete_tag # only local
delete_tag:
	git tag -d $(version)

.PHONY: list
list:
	@grep '^[^#[:space:]].*:' Makefile
