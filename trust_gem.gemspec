# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'trust_gem/version'

Gem::Specification.new do |s|
  s.name          = 'trust_gem'
  s.authors       = ['Joaquin Philippi']
  s.email         = 'jphilippi@trust.lat'
  s.version       = TrustGem::Version
  s.date          = '2020-11-02' # y-m-d
  s.summary       = 'trust_gem'
  # s.files         = `git ls-files`.split("\n")
  # s.require_paths = ['lib/**/*.rb']
  # s.files = Dir['lib/**/*.rb']
  # s.files = Dir['spec/helper.rb', 'lib/**/*.rb']

  s.files = Dir[
      '{app,config,lib,vendor}/**/*',
      'CHANGELOG.md',
      'MIT-LICENSE',
      'README.md',
  ]

  s.require_paths = ['lib']

  #s.add_development_dependency 'generator_spec', '~> 0.9.3'
  #s.add_development_dependency 'rspec', '~> 3.7', '>= 3.7.0'

  s.add_dependency 'railties'
  s.add_dependency 'request_store', '~> 1.7'
  s.add_dependency 'sentry-ruby'
  s.add_dependency 'sentry-rails'
end
