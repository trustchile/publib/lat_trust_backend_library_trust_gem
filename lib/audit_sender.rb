module AuditSender
  require 'net/http'
  require 'net/https'
  require 'oauth_manager'

  module_function

  def sendAudit(transaction, extra_data = {}, remote_ip)

    if transaction[:result].nil? || transaction[:method].nil? || transaction[:operation].nil?
      Rails.logger.info "transaction hash must include 'result', 'method' and 'operation'"
      puts "transaction hash must include 'result', 'method' and 'operation'"
    end

    if ENV['PROJECT_NAME'].nil?
      puts 'PROJECT_NAME ENV is not defined'
      Rails.logger.error 'PROJECT_NAME ENV is not defined'
    end

    headers = {
      'Content-Type' => 'application/json',
      'Authorization' => OauthManager.getAccessToken
    }

    # Set trace headers in headers if are present
    headers = TraceHelper.add_trace_on_hash(headers)

    begin
      req = Net::HTTP::Post.new('/api/v1/audit', headers)
      # puts OauthManager.getAccessToken()
      # add timestamp to the audit
      transaction[:timestamp] = Time.now.getutc.to_i

      req.body = {
        type_audit: 'service',
        application: ENV['PROJECT_NAME'],
        transaction: transaction,
        source: {
          ip: remote_ip,
          api_version: ENV['PROJECT_VERSION'],
          # if resource_owner_id not exist assign empty id
          user_id: !@resource_owner.nil? ? @resource_owner[:id] : ''
        },
        extra_data: extra_data
      }.to_json

      if ENV['AUDIT_URL']
        url = URI.parse(ENV['AUDIT_URL'])

        http = Net::HTTP.new(url.host, url.port)

        # only use https connection if OAUTH_PROVIDER_URL is https url
        if ENV['AUDIT_URL'].start_with?('https')
          http.use_ssl = true
        end

        res = http.start do |http|
          http.request(req)
        end

        Rails.logger.info 'Audit service response code: ' + res.code
        puts 'Audit service response code: ' + res.code
        if res.code != '201'
          # failed audit, report to terminal Input and output of service
          Rails.logger.info 'Audit Input: ' + req.body
          puts 'Audit Input: ' + req.body
          Rails.logger.info 'Audit Response: ' + res.body
          puts 'Audit Response: ' + res.body
        end

        if res.code == '401'
          logger = Rails.logger
          puts 'audit server response a 401 http code, audit not sended'
          logger.error 'audit server response a 401 http code, audit not sended'
        end
      else
        puts 'AUDIT_URL ENV is not defined, audit not sended'
        Rails.logger.error 'AUDIT_URL ENV is not defined, audit not sended'
      end
    # if cannot connect to service by http
    rescue Errno::ECONNREFUSED => e
      Rails.logger.error 'Cannot connect to audit service ' + ENV['AUDIT_URL']
      puts 'Cannot connect to audit service ' + ENV['AUDIT_URL']
    end
  end
end
