module OauthManager
  require 'net/http'
  require 'net/https'
  require 'trace_helper'

  module_function

  #obtain a access token from cache or from oauth2 server
  def getAccessToken
    logger = Rails.logger

    token = Rails.cache.read("internal_access_token")

    if !ENV["OAUTH_PROVIDER_URL"].nil?
      if token.nil?
        #url = URI.parse(ENV["OAUTH_PROVIDER_URL"] + '/oauth/token')
        url = URI.parse(clean_http_url(ENV["OAUTH_PROVIDER_URL"]))

        headers = { 'Content-Type' => 'application/json' }

        # Set trace headers in headers if are present
        headers = TraceHelper.add_trace_on_hash(headers)

        req = Net::HTTP::Post.new('/oauth/token', headers)
        http = Net::HTTP.new(url.host, url.port)

        # only use https connection if OAUTH_PROVIDER_URL is https url
        if ENV["OAUTH_PROVIDER_URL"].start_with?("https")
          http.use_ssl = true
        end

        if ENV['OAUTH_CLIENT_ID'].nil? || ENV['OAUTH_CLIENT_SECRET'].nil?
          logger.error "OAUTH_CLIENT_ID or OAUTH_CLIENT_SECRET ENV is missing, cannot obtain a new access token"
          puts "OAUTH_CLIENT_ID or OAUTH_CLIENT_SECRET ENV is missing, cannot obtain a new access token"
          #return empty access token
          ""
        else
          req.body = {
            client_id: ENV['OAUTH_CLIENT_ID'],
            client_secret: ENV['OAUTH_CLIENT_SECRET'],
            grant_type: "client_credentials"
          }.to_json

          logger.info "Get new access token on " + ENV["OAUTH_PROVIDER_URL"]

          res = http.start do |http|
            http.request(req)
          end

          if res.code == "200"
            #response parse
            response = JSON.parse(res.body)

            #store the access_token in cache to the expire date
            #add 5 minutes less of drift to access_token cache
            Rails.cache.write("internal_access_token", response["access_token"], expires_in: (response["expires_in"].to_i - 60*5))
            "Bearer " + response["access_token"]
          else
            puts "oauth server error response"
            logger.error "oauth server error response"
            puts res.body.to_json
            logger.error res.body.to_json

            ""
          end
        end
      else
        #cached bearer token
        "Bearer " + token
      end

    else
      #set  OAUTH_PROVIDER_URL
      puts "OAUTH_PROVIDER_URL ENV is missing"
      logger.error "OAUTH_PROVIDER_URL ENV is missing"
      ""
    end
  rescue => e
    Sentry.capture_exception(e)
    logger.error "ERROR: Unexpected error: #{JSON.generate(e.message)}"
    ""
  end

  def getHydraToken
    logger = Rails.logger
    token = Rails.cache.read("hydra_internal_access_token")

    if ENV['HYDRA_PUBLIC_URL'].present?
      if token.nil?
        if ENV['HYDRA_CLIENT_ID'].nil? || ENV['HYDRA_CLIENT_SECRET'].nil?
          logger.error "HYDRA_CLIENT_ID or HYDRA_CLIENT_SECRET  is missing"
          puts "HYDRA_CLIENT_ID or HYDRA_CLIENT_SECRET  is missing"
        else
          credentials = ENV['HYDRA_CLIENT_ID'] + ':' + ENV['HYDRA_CLIENT_SECRET']
          encode_credentials = Base64.encode64(credentials).delete("\n")
          url = URI.parse(clean_http_url(ENV['HYDRA_PUBLIC_URL']))
          # Using Authorization Basic with Hydra, client must support 'client_secret_basic' grant_type!
          headers = { 'Content-Type' => 'application/x-www-form-urlencoded', 'Authorization' => "Basic #{encode_credentials}"}

          headers = TraceHelper.add_trace_on_hash(headers)

          req = Net::HTTP::Post.new('/oauth2/token', headers)
          http = Net::HTTP.new(url.host, url.port)

          if ENV['HYDRA_PUBLIC_URL'].start_with?('https')
            http.use_ssl = true
          end

          req.body = "grant_type=client_credentials"
          logger.info "Get new access token on #{ENV['HYDRA_PUBLIC_URL']}"

          res = http.start do |http|
            http.request(req)
          end

          if res.code == "200"
            response = JSON.parse(res.body)
            Rails.cache.write("hydra_internal_access_token", response["access_token"], expires_in:(response["expires_in"].to_i - 60*5))
            "Bearer " + response["access_token"]
          else
            puts "HYDRA Server error response"
            logger.error "HYDRA Server error response"
            puts res.body.to_json
            logger.error res.body.to_json
          end
        end
      else
        "Bearer "+ token
      end
    else
      puts "HYDRA_URL is missing"
      logger.error "HYDRA_URL is missing"
    end
  end

  def clean_http_url(url)
    # parse lat_atena to http://lat_atenea
    url.start_with?('http') ? url : 'http://' + url
  end
end
