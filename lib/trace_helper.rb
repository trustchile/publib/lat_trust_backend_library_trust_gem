module TraceHelper

  module_function

  def get_trace_headers
    RequestStore.store[:trace_headers] || {}
  end

  def add_trace_on_hash(headers = {})
    TraceHelper.get_trace_headers.each { |header, value| headers[header] = value }

    headers
  end

  # obtain trust trace id
  def get_trace_id
    traces = TraceHelper.get_trace_headers
    return nil unless traces.present?

    traces['x-trust-request-id']
  end

  # This method will change the trust trace id
  def set_trace_id(trace_id)
    headers = TraceHelper.get_trace_headers
    headers['x-trust-request-id'] = trace_id
    TraceHelper.set_trace_headers(headers)
  end

  def set_trace_headers(headers)
    RequestStore.store[:trace_headers] = headers
  end

end
