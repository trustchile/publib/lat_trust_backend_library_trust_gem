require 'trust_gem/configuration'
require 'trust_gem/engine'
require 'request_store'

# Main TrustGem namespace.
#
module TrustGem
  class << self
    attr_accessor :logger, :cache

    # logger logic by https://stackoverflow.com/a/30623846
    def logger
      @@logger ||= defined?(Rails) ? Rails.logger : Logger.new(STDOUT)
    end

    def self.logger=(logger)
      @@logger = logger
    end

    def cache
      @cache ||= Rails.cache
    end
  end

  autoload :ScopeCheck,         "trust_gem/scope_check"
  autoload :Utils,              "trust_gem/utils"
  autoload :AuthorizationCheck, "trust_gem/authorization_check"
  autoload :LogEvents,          "trust_gem/log_events"
  autoload :TraceCapture,       "trust_gem/trace_capture"

end



# Example
# Main Doorkeeper namespace.
#
#module Doorkeeper
#  autoload :Errors, "doorkeeper/errors"
#  autoload :GrantFlow, "doorkeeper/grant_flow"
#end
