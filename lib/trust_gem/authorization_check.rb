# frozen_string_literal: true

module TrustGem
  class AuthorizationCheck
    require 'trust_gem/utils'

    def initialize(app)
      @app = app
    end

    def call(env)
      # check bearer token only went rails is in production mode
      if Utils.need_verification?
        query_token = check_token_from_params(env)
        if (env['HTTP_AUTHORIZATION'].present? && env['HTTP_AUTHORIZATION'].start_with?('Bearer')) || query_token.present?
          bearer = env['HTTP_AUTHORIZATION']
          bearer ||= "Bearer #{query_token}"
          res = Utils.checkToken(bearer)

          if res.is_a? Array
            # Si res ya es una respuesta se envia este, en caso contrario se envia un mensaje generico
            res
          elsif res.code != '200'
            error_output = 'Invalid or expired token'
            [
              401, { 'Content-Type' => 'application/json' },
              [{ status: 401, error: error_output }.to_json]
            ]
          else
            body = JSON.parse(res.body)

            RequestStore.store[:token_info] = {
              resource_owner_id: body['resource_owner_id'],
              scopes: body['scope'],
              client_id: body['client']['id'],
              client_company_uid: body['client']['company_id'],
              company_uid: body['company_id'],
              user_companies: body['user_companies'] || []
            }

            RequestStore.store[:token_info][:systemic_token] = RequestStore.store[:token_info][:company_id] == 0

            # if the scopes contains "trust.admin" the company_id becomes to admin id
            if RequestStore.store[:token_info][:scopes].include? ('trust.admin')
              RequestStore.store[:token_info][:systemic_token] == 0
            end

            @status, @headers, @response = @app.call(env)
          end

        else
          error_output = 'Bearer token is required'
          [
            401, { 'Content-Type' => 'application/json' },
            [{ status: 401, error: error_output }.to_json]
          ]
        end
      else
        # dev mode

        # set empty data for token info
        @resource_owner = { id: '' }
        env[:token_info] = {
          resource_owner_id: '',
          scopes: '',
          client_id: ''
        }

        @status, @headers, @response = @app.call(env)
      end
    end

    private

    def check_token_from_params(env)
      uri_string = env['REQUEST_URI']
      uri = URI.parse(uri_string)
      return nil if uri.query.nil?
      query_params = CGI.parse(uri.query)
      return query_params['access_token'].first if query_params['access_token'].present?
      nil
    end
  end
end
