module TrustGem
  module Version
    module_function

    # Gem current version
    #
    # @return [String]
    def to_s
      "2.5.0"
    end
  end
end
