# frozen_string_literal: true

module TrustGem
  module LogEvents
    require 'net/http'
    require 'net/https'
    require 'oauth_manager'

    include Utils

    OPERATION_CREATE = 'CREATE'
    OPERATION_UPDATE = 'UPDATE'
    OPERATION_READ = 'READ'
    OPERATION_DELETE = 'DELETE'

    RELEVANCE_INFO = 'INFO'
    RELEVANCE_IMPORTANT = 'IMPORTANT'
    RELEVANCE_ERROR = 'ERROR'

    OPERATION_LIST = [OPERATION_CREATE, OPERATION_UPDATE, OPERATION_READ, OPERATION_DELETE].freeze
    RELEVANCE_LIST = [RELEVANCE_INFO, RELEVANCE_IMPORTANT, RELEVANCE_ERROR].freeze

    module_function

    # @param [Hash] log_data The data to be send into a service log
    # @option log_data [String] :user_id id of the user who make the operation, is set automatically but can be override and can be null
    # @option log_data [String] :company_id id of company associated to the resource, is set automatically but can be override
    # @option log_data [String] :resource name of the resource involved in the operation, must be in upper case and singular, example: 'DEVICE'
    # @option log_data [String] :operation the operation made to the resource, can be ['CREATE', 'UPDATE', 'READ', 'DELETE']
    # @option log_data [Boolean] :result if the operation is success or not
    # @option log_data [String] :relevance define the importance of this event, can be ['INFO', 'IMPORTANT', 'ERROR']
    # @option log_data [String] :resource_id id of the resource
    # @option log_data [String] :resource_id_2 aux id of the resource, prefer use resource_id
    # @option log_data [String] :description a small text with the description of the event
    # @option log_data [String] :audit_id uuid of the audits associated to this event, only if apply
    # @option log_data [String] :client_id
    # @option log_data [String] :client_ip
    # @option log_data [String] :event_type

    # TODO: permit sync log processing
    # add user company_id
    def send_service_log(log_data = {})
      #TrustGem.logger.info log_data

      token_info = RequestStore.store[:token_info] || {}

      # set this values before run the method inside the new thread!!
      log_data[:trace_uid]    = TraceHelper.get_trace_id
      log_data[:company_id]   = log_data[:company_id] || token_info[:company_uid]
      log_data[:client_id]    = log_data[:client_id] || token_info[:client_id]
      log_data[:user_id]      = log_data[:user_id] || token_info[:resource_owner_id]

      # Set trace headers in the request if are present
      headers = {
        'Content-Type' => 'application/json',
        'Authorization' => OauthManager.getAccessToken
      }
      headers = TraceHelper.add_trace_on_hash(headers)

      #log_data[:client_ip]    = RequestStore.store[:client_id] # add the ip of the origin when is available

      Thread.new do

        begin
          send_log_to_service(log_data, headers)
        rescue => e
          puts "Unknown error trying to send a event: #{e.message}"
          puts e.backtrace
          Sentry.capture_exception(e)
        end

      end
    end

    private

    def send_log_to_service(log_data, headers)
      return unless need_verification? # only send events if is required

      unless log_data.is_a?(Hash)
        raise 'Error on the event data, send_log only must receive a hash as a param'
      end

      unless OPERATION_LIST.include?(log_data[:operation])
        raise 'the event operation field must have a valid value, the valid values are: ' + OPERATION_LIST.join(', ')
      end

      unless RELEVANCE_LIST.include?(log_data[:relevance])
        raise 'the event relevance field must have a valid value, the valid values are: ' + RELEVANCE_LIST.join(', ')
      end

      puts 'PROJECT_NAME ENV is not defined' unless ENV['PROJECT_NAME'].present?

      log_data[:service] = ENV['PROJECT_NAME']

      unless ENV['EVENTS_URL'].present?
        raise 'EVENTS_URL ENV is not defined, event is not send'
      end

      begin
        uri = URI.parse(clean_http_url(ENV['EVENTS_URL'].to_s + '/v1/event/service'))
        http = Net::HTTP.new(uri.host, uri.port)

        request = Net::HTTP::Post.new(uri.path, headers)
        request.body = log_data.to_json

        http.use_ssl = true if ENV['EVENTS_URL'].start_with?('https')

        res = http.request(request)

        if res.code != '201'
          puts "Error detected in event service"
          puts "event service url " + ENV['EVENTS_URL'].to_s
          puts res.to_json
          puts 'Problem sending the request to events service, code response: ' + res.code.to_s
          puts 'Events service response: ' + res.body.to_s
        end

      # if cannot connect to service by http
      rescue Errno::ECONNREFUSED => e
        Sentry.capture_exception(e)
        puts 'Cannot connect to event service ' + ENV['EVENTS_URL']
      end
    end
  end
end
