# frozen_string_literal: true

module TrustGem
  module ScopeCheck
    module_function

    require 'trust_gem/utils'

    # centralized method for validate scopes and company data access,
    # the opts parameter receive the same values as check_scope
    def permissions_check(company_uid, scopes = nil, opts = {})
      # resource_ids = [
      #   {
      #     :id => company_uid.to_s,
      #     :type => 'company_id',
      #   }
      # ]

      resource_ids = {}

      return false unless check_company_permissions(company_uid.to_s, {})

      check_scope(scopes, resource_ids, opts)
    end

    def check_company_permissions(company_id, _opts = {})
      return true unless TrustGem::Utils.need_verification?
      # check if is systemic token or is a user access token / integration token
      return true if TrustGem::Utils.token_company_uid == '0'
      if company_id.present?
        return true if TrustGem::Utils.token_company_uid == company_id.to_s
        return true if TrustGem::Utils.token_user_companies.include?(company_id.to_s)
      end
      # if opts.fetch(:auto_response, false)
        # render error
        # forbidden_render('Insufficient permission')
      # end

      TrustGem.logger.warn 'Invalid permissions for the request credentials'
      false
    end

    # receive an array of scopes
    # returns: boolean
    # usage: check_scope(['api_test.users.r'], )
    # permitted opts: {partial_match: bool}
    # partial_match defines if at least 1 scope has to match with the token's scopes
    def check_scope(scopes = [], resource_ids = nil, opts = {})
      return true unless TrustGem::Utils.need_verification?

      bearer = request.headers['Authorization']
      bearer ||= "Bearer #{request.params['access_token']}"
      partial_match = opts[:partial_match] || false
      scopes = scopes.join(' ');
      server = opts[:server] || nil
      dni = opts[:dni] || nil

      token_info = Utils.checkToken(bearer, server, scopes, resource_ids, partial_match, dni)

      token_info = JSON.parse(token_info.body)

      return token_info['code'] == 200
    rescue => e
      Sentry.capture_exception(e)
      Rails.logger.error("ERROR: #{e.message}")
      return false
    end

  end
end
