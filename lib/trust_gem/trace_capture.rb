module TrustGem
  class TraceCapture
    #require 'trace_helper'

    def initialize(app)
      @app = app
    end

    def call(env)
      # Store in global variables the traces headers if are present
      headers = {}

      # Copy trace id in global variables
      headers['x-request-id']       = env['HTTP_X_REQUEST_ID']
      headers['x-b3-traceid']       = env['HTTP_X_B3_TRACEID']
      headers['x-b3-spanid']        = env['HTTP_X_B3_SPANID']
      headers['x-b3-parentspanid']  = env['HTTP_X_B3_PARENTSPANID']
      headers['x-b3-sampled']       = env['HTTP_X_B3_SAMPLED']
      headers['x-b3-flags']         = env['HTTP_X_B3_FLAGS']
      headers['x-ot-span-context']  = env['HTTP_X_OT_SPAN_CONTEXT']

      headers['x-trust-request-id'] = env['HTTP_X_TRUST_REQUEST_ID'] # Trust Internal Trace

      RequestStore.store[:trace_headers] = headers.compact

      @status, @headers, @response = @app.call(env)
    end
  end
end
