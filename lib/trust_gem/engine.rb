module TrustGem
  #include RequestStore

  class Engine < ::Rails::Engine
    class << self
      attr_accessor :logger

      def logger
        @logger ||= Logger.new($stdout).tap do |log|
          log.progname = self.name
        end
      end
    end

    config.autoload_paths += Dir["#{config.root}/lib/**/"]
  end
end