module TrustGem
  module Utils
    module_function

    require 'net/http'
    require 'net/https'

    require 'sentry-ruby'
    require 'sentry-rails'

    # TODO: change name
    # check if the application require security verifications (token, scopes, etc)
    def need_verification?
      # in future add more methods for different framework and rules
      Rails.env.production? || ENV['FORCE_AUTHENTICATION'] == 'true'
    end

    def check_token_resource_owner_id(user_id_to_check)
      token_user_id = token_resource_owner_id
      
      return user_id_to_check == token_user_id unless token_user_id.eql?(false)

      false
    end

    def token_client_id
      return RequestStore.store[:token_info][:client_id] if RequestStore.store[:token_info].present?

      false
    end

    def token_resource_owner_id
      return RequestStore.store[:token_info][:resource_owner_id] if RequestStore.store[:token_info].present?

      false
    end

    def token_company_uid
      return RequestStore.store[:token_info][:company_uid] if RequestStore.store[:token_info].present?

      false
    end

    def token_user_companies
      return RequestStore.store[:token_info][:user_companies] if RequestStore.store[:token_info].present?

      false
    end

    def token_scopes
      if token_info_present?
        return RequestStore.store[:token_info][:scopes] if RequestStore.store[:token_info][:scopes].is_a? Array
      end

      []
    end

    def is_admin_token?
      if self.need_verification?
        token_company_uid == '0' || token_company_uid == 'admin'
      else
        true
      end
    end

    def token_info_present?
      RequestStore.store[:token_info].present?
    end

    def clean_http_url(url)
      # parse lat_atena to http://lat_atenea
      url = 'http://' + url unless url.start_with?('http')
      # remove spaces and normalized url characters
      url.strip
    end

    def self.generate_resource_object(ids, type)
      resource_ids = []
      ids.each do |id|
        resource_ids << {
          'id' => id,
          'type' => type
        }
      end

      resource_ids
    end

    def checkToken(bearer, server = nil, scopes = nil, resource_ids = nil, partial_match = false, dni = nil)
      if !ENV['JANUS_URL'].nil?
        oauth_url = ENV['JANUS_URL'].start_with?('http') ? ENV['JANUS_URL'] : 'http://' + ENV['JANUS_URL']
        url = URI.parse(oauth_url + '/api/v1/auth')
        bearer = bearer.split(' ')[1]

        form_data = {
          'token' => bearer
        }

        form_data['server'] = server unless server.nil?
        form_data['scopes'] = scopes unless scopes.nil?
        form_data['resource_ids'] = resource_ids.to_json unless resource_ids.nil?
        form_data['partial_match'] = partial_match
        form_data['dni'] = dni

        # Set trace headers in headers if are present
        headers = TraceHelper.add_trace_on_hash()

        req = Net::HTTP::Post.new(url.path)
        req.set_form_data(form_data)
        https = Net::HTTP.new(url.host, url.port)

        # only use https connection if JANUS_URL is https url
        https.use_ssl = true if ENV['JANUS_URL'].start_with?('https')
        starting = Time.now

        res = https.start do |_http|
          https.request(req)
        end

        res
      else
        error_output = 'JANUS_URL ENV is not set on ' + ENV['PROJECT_NAME']
        [
          401, { 'Content-Type' => 'application/json' },
          [{ status: 401, error: error_output }.to_json]
        ]
      end
    rescue => e
      Sentry.capture_exception(e)
      Rails.logger.error("ERROR: #{e.message}")
      [
        500, { 'Content-Type' => 'application/json' },
        [{ status: 500, error: e.message }.to_json]
      ]
    end

    def get_request_client_ip
      return request.headers["X-Envoy-External-Address"]
    end
  end
end
