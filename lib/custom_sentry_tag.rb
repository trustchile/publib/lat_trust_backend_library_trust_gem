module CustomSentryTag
  # receives an array with parameter names,
  # if inside the request there is a field with one of those names,
  # then it is added to the sentry tags
  def sentry_tags(params_key = [])
    begin
      register_trust_tags

      params_key.each do |key_name|
        if key_name.include? '.'
          value = nil
          hash = params
          # search for keys nested in the params
          key_name.split('.').each do |key|
            value = hash[key]
            if value.present?
              key_name = key
              hash = hash[key]
            else
              # key not found into hash, impossible to continue
              value = nil
              hash = nil
              key_name = nil
              break
            end
          end
        else
          value = params[key_name]
        end

        Sentry.tags_context key_name => value if value.present?
      end
    rescue => ex
      Sentry.capture_exception(ex)
      # TODO: manejar caso de que el dni no exista, actualmente esta levantando una ex
      Rails.logger.warn 'Error on TrustGem::set_context_tags'
      Rails.logger.warn ex.message
    end
  end

  # Add sentry tags with token info
  def register_trust_tags
    Sentry.tags_context resource_owner_id => @token_info[:resource_owner_id] if @token_info[:resource_owner_id].present?
    Sentry.tags_context client_company_uid => @token_info[:client_company_uid] if @token_info[:client_company_uid].present?
    Sentry.tags_context company_uid => @token_info[:company_uid] if @token_info[:company_uid].present?
  end
end
